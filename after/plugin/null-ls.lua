require("lsp-format").setup {}

local null_ls = require("null-ls")

local formatting = null_ls.builtins.formatting
local on_attach = function (client)
  require("lsp-format").on_attach(client)
end

require("lspconfig").gopls.setup { on_attach = on_attach }

null_ls.setup({
  sources = {
    formatting.eslint,
    formatting.stylua,
    formatting.prettier,
  },
  on_attach = on_attach
})
