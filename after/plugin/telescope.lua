require("telescope").load_extension("harpoon")
require("telescope").load_extension("file_browser")

require("telescope").setup({
	defaults = {
		vimgrep_arguments = {
			"rg",
			"--color=never",
			"--no-heading",
			"--with-filename",
			"--line-number",
			"--column",
			"--smart-case",
			"--hidden",
			"--no-ignore",
		},
		file_ignore_patterns = {
			-- "node_modules",
			".git",
			"build",
			"dist",
			"vendor",
		},
	},
	pickers = {},
	extensions = {
		file_browser = {
			theme = "ivy",
			depth = 2,
		},
	},
})

local builtin = require("telescope.builtin")

vim.keymap.set("n", "<leader>p", builtin.find_files, {})
vim.keymap.set("n", "<leader>g", builtin.git_status, {})
vim.keymap.set("n", "<leader>gh", builtin.git_commits, {})
vim.keymap.set("n", "<leader>e", builtin.buffers, {})
vim.keymap.set("n", "<leader>fh", builtin.oldfiles, {})
vim.keymap.set("n", "<leader>r", builtin.lsp_references, {})
vim.keymap.set("n", "<leader>l", builtin.live_grep, {})
