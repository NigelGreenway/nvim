local lsp = require("lsp-zero")
local keymap = vim.keymap

lsp.preset("recommended")

lsp.ensure_installed {
	'tsserver',
	'eslint',
}

local cmp = require 'cmp'
local cmp_select = { behaviour = cmp.SelectBehavior.Select }
local cmp_mappings = lsp.defaults.cmp_mappings({
	['<C-k>'] = cmp.mapping.select_prev_item(cmp_select),
	['<C-j>'] = cmp.mapping.select_next_item(cmp_select),
	['<space>'] = cmp.mapping.confirm({ select = true }),
	['<C-space>'] = cmp.mapping.complete(),
})

lsp.set_preferences({
	sign_icons = {}
})

lsp.setup_nvim_cmp({
	mappings = cmp_mappings
})

lsp.on_attach(function(client, bufnr)
	local opts = { buffer = bufnr, remap = false }

	keymap.set("n", ",d", function() vim.lsp.buf.definition() end, opts)
	keymap.set("n", "K", function() vim.lsp.buf.hover() end, opts)
	keymap.set("n", ",s", function() vim.lsp.buf.workspace_symbol() end, opts)
	keymap.set("n", ",,", function() vim.diagnostic.open_float() end, opts)
	keymap.set("n", "]d", function() vim.diagnostic.goto_next() end, opts)
	keymap.set("n", "[d", function() vim.diagnostic.goto_prev() end, opts)
	keymap.set("n", "<leader>vca", function() vim.lsp.buf.code_action() end, opts)
  -- keymap.set("n", ",r", vim.builtin.lsp_references, {})
	keymap.set("n", ",rn", function() vim.lsp.buf.rename() end, opts)
	keymap.set("n", ",s", function() vim.lsp.buf.signature_help() end, opts)
end)

lsp.setup()
