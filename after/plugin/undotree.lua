local keymap = vim.keymap
local cmd = vim.cmd

keymap.set("n", "<leader>u", cmd.UndotreeToggle)
