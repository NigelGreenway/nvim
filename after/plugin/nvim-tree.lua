require('nvim-tree').setup {
  filters = {
    custom = {
      "^\\.git$"
    }
  },
  filesystem_watchers = {
    enable         = true,
    debounce_delay = 100
  },
  git = {
    ignore = false,
    timeout = 1000
  },
  sync_root_with_cwd = true,
  update_focused_file = {
    enable = true
  },
  view = {
    width = 40,
  }
}
