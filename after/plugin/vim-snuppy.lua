require('snippy').setup({
    mappings = {
        is = {
            [']'] = 'expand_or_advance',
            ['['] = 'previous',
        },
        nx = {
            ['<leader>x'] = 'cut_text',
        },
    },
})
