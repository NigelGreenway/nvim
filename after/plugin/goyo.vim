function! s:goyo_enter()
  set wrap
  set nu
  Goyo 140
endfunction

function! s:goyo_leave()
  hi LineNr ctermfg=8
endfunction

autocmd! User GoyoEnter nested call <SID>goyo_enter()
autocmd! User GoyoLeave nested call <SID>goyo_leave()

