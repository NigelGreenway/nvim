local keymap = vim.keymap

-- Window/pane management
keymap.set("n", "<C-w>m", ":MaximizerToggle<CR>")

-- Buffer management
keymap.set("n", "<leader>.", ":bnext<CR>")
keymap.set("n", "<leader>,", ":bprevious<CR>")
keymap.set("n", "<leader>b", "<C-^>") -- Toggle between current and previous buffer
keymap.set("n", "<leader>bd", ":bd<CR>") -- Close current buffer
keymap.set("n", "<leader>bda", ":%bd|e#<CR>") -- Close all buffers but current one

-- Tab management
keymap.set("n", ",<Tab>", ":tabnext<CR>")
keymap.set("n", ",<S-Tab>", ":tabprevious<CR>")
keymap.set("n", "<C-t>", ":tabclose<CR>")

-- Move highlighted text up/down
keymap.set("v", "J", ":m '>+1<CR>gv=gv")
keymap.set("v", "K", ":m '<-2<CR>gv=gv")

-- Keep cursor where it is when joining lines
keymap.set("n", "J", "mzJ`z")

-- Keep focus in the center of screen when searching
keymap.set("n", "<C-d>", "<C-d>zz")
keymap.set("n", "<C-u>", "<C-u>zz")

-- Allow search terms to stay in the center of the screen
keymap.set("n", "n", "nzzzv")
keymap.set("n", "N", "Nzzzv")

keymap.set("n", ",t", ":vsplit <Cr>%<Tab>")

keymap.set("i", "<C-c>", "<Esc>")

-- Config: choosewin
keymap.set("n", "-", ":ChooseWin<CR>")

-- Config: vim-test
keymap.set("n", "<F6>", ":TestNearest<CR>")
keymap.set("n", "<F7>", ":TestFile<CR>")

-- Config: nvim-tree
keymap.set("n", "<leader>n", ":NvimTreeToggle<CR>") -- Disabled as using Telescope instead
keymap.set("n", ",f", ":NvimTreeFindFile<CR>")

keymap.set("n", "<F5>", ":UndotreeToggle<CR>")

keymap.set("n", "<leader>xx", ":TroubleToggle workspace_diagnostics<CR>")

keymap.set("n", "<leader><Tab>", ":Goyo<CR>")

-- Config: telescope
local actions = require("telescope.actions")

require("telescope").setup({
	defaults = {
		mappings = {
			i = {
				["<C-?>"] = actions.which_key,
				["<C-k>"] = actions.move_selection_previous,
				["<C-j>"] = actions.move_selection_next,
				["<C-q>"] = actions.send_selected_to_qflist + actions.open_qflist,
			},
		},
	},
})
keymap.set("n", "<leader>a", ":Telescope find_files hidden=true no_ignore=true<CR>")
keymap.set("n", "<leader>*", ":Telescope grep_string search=<C-R><C-W><CR>")
-- keymap.set("n", "<leader>n", ":Telescope file_browser path=%:p:h select_buffer=true<CR>")
