local set = vim.opt
local let = vim.o

vim.g.mapleader = " "

set.tabstop = 2
set.softtabstop = 2
set.shiftwidth = 2
set.expandtab = true
set.smartindent = true

set.wrap = false

set.compatible = false
set.backspace = "indent,eol,start"
set.showcmd = true
set.autoread = true
set.hidden = true
set.number = true

-- set.listchar.tab = '⟶'
-- set.listchar.space = '·'
-- trail = '·',
-- extends = '>',
-- precedes = '<',
-- nbsp = '%',
set.list = true

set.foldmethod = "indent"
set.foldlevel = 3

set.cursorline = true
-- set.rule = true
set.wildmenu = true
set.title = true
set.cursorcolumn = true

set.swapfile = false
set.backup = false
set.undodir = os.getenv("HOME") .. "/.vim/undodir"
set.undofile = true

set.incsearch = true
set.ignorecase = true
set.smartcase = true
set.hlsearch = false

set.termguicolors = true

set.mouse = "a"

set.encoding = "utf-8"
set.linebreak = true
set.scrolloff = 10

set.updatetime = 50

set.confirm = true
set.clipboard = "unnamedplus"
set.spelllang = "en"
set.spellfile = os.getenv("HOME") .. "/.config/nvim/spell/en.utf-8.add"
set.spell = true

vim.g.choosewin_overlay_enable = true

vim.g.netrw_winsize = 15

let.test_strategy = "vimux"

local prettier = {
	formatCommand = 'prettierd "${INPUT}"',
	formatStdin = true,
	env = {
		string.format(
			"PRETTIERD_DEFAULT_CONFIG=%s",
			vim.fn.expand("~/.config/nvim/utils/linter-config/.prettierrc.json")
		),
	},
}
