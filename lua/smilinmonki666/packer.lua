-- Only required if you have packer configured as `opt`
vim.cmd([[packadd packer.nvim]])

return require("packer").startup(function(use)
	-- Packer can manage itself
	use("wbthomason/packer.nvim")

	use({
		"nvim-telescope/telescope.nvim",
		requires = { { "nvim-lua/plenary.nvim" } },
	})

	use({
		"catppuccin/nvim",
		as = "catppuccin",
		config = function()
			vim.cmd("colorscheme catppuccin")
		end,
	})

	use("nvim-treesitter/nvim-treesitter", { run = ":TSUpdate" })

	use("theprimeagen/harpoon")
	use("mbbill/undotree")

	use({
		"VonHeikemen/lsp-zero.nvim",
		branch = "v1.x",
		requires = {
			-- LSP Support
			{ "neovim/nvim-lspconfig" }, -- Required
			{ "williamboman/mason.nvim" }, -- Optional
			{ "williamboman/mason-lspconfig.nvim" }, -- Optional

			-- Autocompletion
			{ "hrsh7th/nvim-cmp" }, -- Required
			{ "hrsh7th/cmp-nvim-lsp" }, -- Required
			{ "hrsh7th/cmp-buffer" }, -- Optional
			{ "hrsh7th/cmp-path" }, -- Optional
			{ "saadparwaiz1/cmp_luasnip" }, -- Optional
			{ "hrsh7th/cmp-nvim-lsp" }, -- Optional
			{ "hrsh7th/cmp-nvim-lua" }, -- Optional

			-- Snippets
			{ "L3MON4D3/LuaSnip" }, -- Required
			{ "rafamadriz/friendly-snippets" }, -- Optional
			{ "dcampos/cmp-snippy" },
		},
	})

	use({ "tpope/vim-commentary" })
	use({ "tpope/vim-surround" })
	use({ "tpope/vim-fugitive" })
	use({ "airblade/vim-gitgutter" })
	use({ "editorconfig/editorconfig-vim" })
	use({ "t9md/vim-choosewin" })

	use({ "othree/html5.vim" })
	use({ "mattn/emmet-vim" })
	use({ "vim-test/vim-test" })
	use({ "rstacruz/vim-closer" })
	use({ "ntpeters/vim-better-whitespace" })
	use({ "easymotion/vim-easymotion" })
	use({ "szw/vim-maximizer" })

	use({
		"nvim-tree/nvim-tree.lua",
		requires = {
			"nvim-tree/nvim-web-devicons",
		},
		config = function()
			require("nvim-tree").setup({})
		end,
	})

	use({ "jose-elias-alvarez/null-ls.nvim" })
	use({ "lukas-reineke/lsp-format.nvim" })

	use({ "ap/vim-css-color" })
	use({ "dcampos/nvim-snippy" })

	use({
		"s1n7ax/nvim-search-and-replace",
		config = function()
			require("nvim-search-and-replace").setup()
		end,
	})

	use({ "elzr/vim-json" })
	use({ "matze/vim-move" })

	use({
		"folke/trouble.nvim",
		requires = "nvim-tree/nvim-web-devicons",
		config = function()
			require("trouble").setup({
				-- your configuration comes here
				-- or leave it empty to use the default settings
				-- refer to the configuration section below
			})
		end,
	})

	use({ "junegunn/goyo.vim" })

	use({ "prisma/vim-prisma" })

	use({ "kburdett/vim-nuuid" })

	use({
		"nvim-telescope/telescope-file-browser.nvim",
		requires = { "nvim-telescope/telescope.nvim", "nvim-lua/plenary.nvim" },
	})
end)
